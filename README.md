# eventsdaily

Eventsdaily is a collaborative event based Flutter application.


## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).


## TODOs
- Event list
- Event detail page
- Submit event
- View/edit saved event
- Share/rate/flag event
- Search/tags search event
- Add map to event detail page
